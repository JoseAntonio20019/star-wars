const but = document.getElementById('playButton');
const selection = document.getElementById('musicSelect');
const audio_imperialMarch = new Audio('./audio/imperialMarch.mp3');
const audio_mandalorian = new Audio('./audio/mandalorian.mp3');
let counter = 0;

but.addEventListener('click', () => {
  counter++;

  switch (selection.value) {
    case '0':
      if(counter % 2 == 0){
        but.classList = 'bi bi-pause-fill';
        audio_mandalorian.loop = true;
        audio_mandalorian.play();
        
      } else {
        but.classList = 'bi bi-play-fill';
        audio_mandalorian.pause();
      }
      break;
    case '1':
      if(counter % 2 == 0){
        but.classList = 'bi bi-pause-fill';
        audio_imperialMarch.loop = true;
        audio_imperialMarch.play();
        
      } else {
        but.classList = 'bi bi-play-fill';
        audio_imperialMarch.pause();
      }
      break;
  }
});

selection.addEventListener('change', () => {
  counter = 0;
  but.classList = 'bi bi-play-fill';

  switch (selection.value) {
    case '0':
      audio_imperialMarch.pause();
      break;
    case '1':
      audio_mandalorian.pause();
      break;
  }
});